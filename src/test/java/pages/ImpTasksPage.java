package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import suite.SuiteManager;
import util.DriverManager;

public class ImpTasksPage extends SuiteManager {

    public ImpTasksPage() {
        PageFactory.initElements(DriverManager.driver, this);
    }

    @FindBy(xpath = "//div[contains(text(),'Important Tasks')]/../..")
    WebElement impTasksPageMenu;

    public void impTasksPage() {
        impTasksPageMenu.click();
    }
}
