package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import suite.SuiteManager;
import util.DriverManager;

public class AllTasksPage extends SuiteManager {

    public AllTasksPage() {
        PageFactory.initElements(DriverManager.driver, this);
    }

    @FindBy(xpath = "//mat-icon[contains(text(),'home')]")
    WebElement allTasksPageMenu;

    public void availableTasks() {
        allTasksPageMenu.click();

    }

}
