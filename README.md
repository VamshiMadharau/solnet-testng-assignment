# TestNG Test Framework 

This framework is used to login and verify the Solnet Angular based UI. It is written in Java using Selenium WebDriver. 


## Getting Started

To get started install the required software and run the test to verify.

### Prerequisites


Install the below prerequisites

```
- Java v11
- Maven 
- Any IDE(Preferably IntelliJ)
```

## Running the tests
To run the test, 
- provide website url, Username/ Password along with the Test Input data details
``` in src/main/resources/config/testdata.properties``` 

- Run using maven 
    - Open the project in terminal and run the command
        ```$mvn clean install```

- Running tests through IDE
    - Import the project to IntelliJ IDE, build the project. Right Click on the testing.xml file and Run the Tests.
    - Class name to run the tests is defined in testng.xml file and the Tests are defined under ```/src/test/java/tests/LoginTest``` along with the coding documentation to get an understanding on what tests are running and the validations for each page.
    - Executed tests screenshots are captured while testing and saved under ```src/test/java/screenshots/``` 
    
- Things to Note
    - Solution is now running on ChromeDriver located in ```src/java/resources/drivers/```and can be extended for other browsers by adding drivers into this folder and replacing the DriverManager in ```src/java/util/Drivermanager``` ChromeDriver initialisation to FirefoxDriver initialisation.    