
package tests;

import org.testng.Assert;
import org.testng.annotations.*;
import pages.AllTasksPage;
import pages.HomePage;
import pages.ImpTasksPage;
import pages.LoginPage;
import suite.SuiteManager;
import util.DriverManager;

import java.io.IOException;


public class LoginTest extends SuiteManager {

    public LoginPage loginPage;
    public HomePage homePage;
    public AllTasksPage allTasksPage;
    public ImpTasksPage impTasksPage;
    String screenshotsPath = System.getProperty("user.dir")+"/"+"/src/test/java/screenshots/";

    @Test(description = "Login Test", priority = 1)
    public void login() throws Exception {
        loginPage = new LoginPage();
        homePage = loginPage.login();
        DriverManager.takeSnapShot(DriverManager.driver, screenshotsPath + "2_HomePageScreen.png");
        Assert.assertEquals(homePage.validateHomePage(), true);
    }

    @Test(description = "Add a Task Test with 'star (*)' and Validate it's been added", priority = 2)
    public void addTask() throws IOException {
        homePage.addTask();
        DriverManager.takeSnapShot(DriverManager.driver, screenshotsPath + "3_AddedTaskOne.png");
        homePage.validateAddedTask();
    }

    @Test(description = "Remove a Task Test and Validate it's been removed", priority = 3)
    public void removeTask() throws IOException {
        homePage.removeTask();
        DriverManager.takeSnapShot(DriverManager.driver, screenshotsPath + "5_AddedTaskTwo_After_Removal.png");
        homePage.validateRemovedTask();
    }

    @Test(description = "Validate All Task Page", priority = 4)
    public void validateAllTasks() throws IOException {
        allTasksPage = new AllTasksPage();
        allTasksPage.availableTasks();
        homePage.validateAddedTask();
        homePage.validateAddedAnotherTask();
        DriverManager.takeSnapShot(DriverManager.driver, screenshotsPath + "6_AllTasksPage.png");
    }

    @Test(description = "Validate Important Task Page", priority = 5)
    public void validateImpTasksPage() throws IOException {
        impTasksPage = new ImpTasksPage();
        impTasksPage.impTasksPage();
        DriverManager.takeSnapShot(DriverManager.driver, screenshotsPath + "7_ImpTasksPage.png");
        homePage.validateAddedTask();
    }

    @Test(description = "Validate Completed (Marked) Task", priority = 6)
    public void validateCompletedTask() throws IOException {
        homePage.validateCompletedTask();
    }

    @Test(description = "Validate Open (UnMarked) Task", priority = 7)
    public void validateUnmarkedTask() throws IOException {
        homePage.validateUnMarkedTask();
    }
}