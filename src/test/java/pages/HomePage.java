package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import suite.SuiteManager;
import util.DriverManager;

import java.io.IOException;

public class HomePage extends SuiteManager {

    String screenshotsPath = System.getProperty("user.dir")+"/"+"/src/test/java/screenshots/";

    public HomePage() {
        PageFactory.initElements(DriverManager.driver, this);
    }

    @FindBy(xpath = "//a//div[contains(text(),'My day')]")
    WebElement myDayMenu;

    @FindBy(xpath = "//input[@name='taskTitle']")
    WebElement taskTitleInput;

    @FindBy(xpath = "//input[@name='taskDesc']")
    WebElement taskDescription;

    @FindBy(css = ".mat-checkbox .mat-checkbox-label")
    WebElement isImportant;

    @FindBy(xpath = "//span[contains(text(),'Add')]")
    WebElement addTask;

    @FindBy(css = "span.completed-task")
    WebElement completedTaskElement;

    @FindBy(xpath = "//mat-icon[contains(text(),'logout')]")
    WebElement logout;

    String taskId = "//mat-card//span[.='";
    String taskNameRemovalId = "//span[contains(text(),'";
    String closeClick = "')]/../mat-icon[contains(text(),'close')]";
    String checkbox = "')]/../mat-checkbox";
    String completedTaskClass = "//span[@class='completed-task'][contains(text(),'";
    String unMarkedTaskClass = "//span[@class=''][contains(text(),'";


    String taskOne = config.getProperty("TaskTitle1");
    String TaskDesc1 = config.getProperty("TaskDesc1");
    String taskTwo = config.getProperty("TaskTitle2");
    String TaskDesc2 = config.getProperty("TaskDesc2");


    public boolean verifyTaskName(String TaskName) {
        return DriverManager.driver.findElement(By.xpath(taskId + TaskName + "']")).isDisplayed();
    }

    public boolean verifyRemovedTaskName(String TaskName) {
        int size =  DriverManager.driver.findElements(By.xpath(taskId + TaskName + "']")).size();
        if(size == 0) {
            return true;
        } else {
            return false;
        }
    }

    public void addTask() {
        myDayMenu.click();
        taskTitleInput.sendKeys(taskOne);
        taskDescription.sendKeys(TaskDesc1);
        isImportant.click();
        addTask.click();
    }

    public boolean validateHomePage() {
        return logout.isDisplayed()
                && myDayMenu.isDisplayed();
    }

    public void validateAddedTask() {
        verifyTaskName(taskOne);
    }

    public void validateAddedAnotherTask() {
        addAnotherTask();
        verifyTaskName(taskTwo);

    }

    public void addAnotherTask() {
        myDayMenu.click();
        taskTitleInput.sendKeys(taskTwo);
        taskDescription.sendKeys(TaskDesc2);
        addTask.click();
    }

    public void removeTask() throws IOException {
        addAnotherTask();
        DriverManager.takeSnapShot(DriverManager.driver, screenshotsPath + "4.AddedTaskTwo_Before_Removal.png");
        verifyTaskName(taskTwo);
        DriverManager.driver.findElement(By.xpath(taskNameRemovalId + taskTwo + closeClick)).click();
    }

    public void validateRemovedTask() {
        verifyRemovedTaskName(taskTwo);
    }

    public void completeTask() {
        myDayMenu.click();
        DriverManager.driver.findElement(By.xpath(taskNameRemovalId + taskTwo + checkbox)).click();
    }

    public void unMarkCompletedTask() {
        myDayMenu.click();
        DriverManager.driver.findElement(By.xpath(completedTaskClass + taskTwo + checkbox)).click();
    }

    public boolean validateCompletedTask() throws IOException {
        completeTask();
        DriverManager.takeSnapShot(DriverManager.driver, screenshotsPath + "8_Completed_MarkedTask.png");
        String text = completedTaskElement.getText();
        if(text.equals(taskTwo)) {
            return true;
        }
        return false;
    }

    public boolean validateUnMarkedTask() throws IOException {
        unMarkCompletedTask();
        DriverManager.takeSnapShot(DriverManager.driver, screenshotsPath + "9_UnMark_CompletedTask.png");
        return DriverManager.driver.findElement(By.xpath(unMarkedTaskClass + taskTwo + checkbox)).isDisplayed();
    }
}



