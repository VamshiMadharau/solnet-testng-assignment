package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import suite.SuiteManager;
import util.DriverManager;

public class LoginPage extends SuiteManager {

    String screenshotsPath = System.getProperty("user.dir")+"/"+"/src/test/java/screenshots/";

    public LoginPage() {
        PageFactory.initElements(DriverManager.driver, this);
    }

    @FindBy(xpath = "//input[@type='email']")
    private WebElement usernameField;

    @FindBy(xpath = "//input[@type='password']")
    private WebElement passwordField;

    @FindBy(xpath = "//span[contains(text(),'Login')]")
    private WebElement submitButton;

    String username = config.getProperty("username");
    String password = config.getProperty("password");

    public void enterValue(WebElement field, String value) {
        field.click();
        field.clear();
        field.sendKeys(value);
    }

    public void clickButton(WebElement button) {
        button.click();
    }

    public HomePage login() throws Exception {
        enterValue(usernameField, username);
        enterValue(passwordField, password);
        DriverManager.takeSnapShot(DriverManager.driver, screenshotsPath + "1_LoginScreen.png");
        clickButton(submitButton);
        return new HomePage();
    }

}




